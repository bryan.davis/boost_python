
struct Test
{
    Test(int a) : a_{a} {}
    void set(int a) { a_ = a; }
    int get() const { return a_; }
    int square() const;
private:
    int a_;
};

