#include <boost/python.hpp>

#include "Test.h"
int Test::square() const
{
    return a_*a_;
}

using namespace boost::python;
BOOST_PYTHON_MODULE(Test_ext)
{
    class_<Test>("Test", init<int>())
        .def("get",    &Test::get   )
        .def("set",    &Test::set   )
        .def("square", &Test::square)
    ;
}


