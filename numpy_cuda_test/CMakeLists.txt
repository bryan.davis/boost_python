cmake_minimum_required(VERSION 3.5.0)

project(square_ext LANGUAGES C CXX CUDA)

find_package(PythonLibs 3.6 REQUIRED)
find_package(Boost 1.54.0 REQUIRED)

add_library (
    square_ext SHARED
    src/CudaApi/CudaPtr.cu
    src/square.cu
)

set_target_properties(
   square_ext
   PROPERTIES
   	PREFIX ""
	CUDA_SEPARABLE_COMPILATION ON
)

target_compile_features (
    square_ext
    PRIVATE
        cxx_std_11
)

include_directories( 
    include
    ${PYTHON_INCLUDE_DIRS} 
    ${Boost_INCLUDE_DIRS} 
)

target_link_libraries(
    square_ext
    python3.6m
    boost_python3 
    boost_numpy3 
)

