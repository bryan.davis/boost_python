#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import build.square_ext as sq

x = np.arange(np.pi, 5000000., np.sqrt(2), dtype=np.float32)
y = sq.square(x)
print('size of x:', x.size)
print('square<float> passes:', all(x**2 == y))

x = np.arange(np.pi, 5000000., np.sqrt(2), dtype=np.float64)
y = sq.square(x)
print('size of x:', x.size)
print('square<double> passes:', all(x**2 == y))

x = np.arange(np.pi, 500., np.sqrt(.00000002), dtype=np.complex64) + 1j*np.arange(-5*np.pi, -500. - 4*np.pi, -np.sqrt(.00000002), dtype=np.complex64)
y = sq.square(x)
x2 = x**2
print('size of x:', x.size)
print('square<complex64> passes:', all(np.abs(np.real(x**2) - np.real(y)) <= 0.1))
print('square<complex64> passes:', all(np.abs(np.imag(x**2) - np.imag(y)) <= 0.1))


x = np.array([5],dtype=complex)
try:
    y = sq.square(x)
    raise AssertionError('FAILURE: should have thrown exception')
except RuntimeError:
    print('Exception test passes')
    pass

