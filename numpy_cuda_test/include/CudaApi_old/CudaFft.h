#pragma once

#include <exception>
#include <cufft.h>

inline const char* cudaFftErrorString(cufftResult err)
{
    switch (err)
    {
        case CUFFT_SUCCESS:
            return "CUFFT_SUCCESS";
        case CUFFT_INVALID_PLAN:
            return "CUFFT_INVALID_PLAN";
        case CUFFT_ALLOC_FAILED:
            return "CUFFT_ALLOC_FAILED";
        case CUFFT_INVALID_TYPE:
            return "CUFFT_INVALID_TYPE";
        case CUFFT_INVALID_VALUE:
            return "CUFFT_INVALID_VALUE";
        case CUFFT_INTERNAL_ERROR:
            return "CUFFT_INTERNAL_ERROR";
        case CUFFT_EXEC_FAILED:
            return "CUFFT_EXEC_FAILED";
        case CUFFT_SETUP_FAILED:
            return "CUFFT_SETUP_FAILED";
        case CUFFT_INVALID_SIZE:
            return "CUFFT_INVALID_SIZE";
        case CUFFT_UNALIGNED_DATA:
            return "CUFFT_UNALIGNED_DATA";
        case CUFFT_INCOMPLETE_PARAMETER_LIST:
            return "CUFFT_INCOMPLETE_PARAMETER_LIST";
        case CUFFT_INVALID_DEVICE:
            return "CUFFT_INVALID_DEVICE";
        case CUFFT_PARSE_ERROR:
            return "CUFFT_PARSE_ERROR";
        case CUFFT_NO_WORKSPACE:
            return "CUFFT_NO_WORKSPACE";
        case CUFFT_NOT_IMPLEMENTED:
            return "CUFFT_NOT_IMPLEMENTED";
        case CUFFT_LICENSE_ERROR:
            return "CUFFT_LICENSE_ERROR";
        default:
            return "CUFFT_UNKNOWN_ERROR";
    }
}

inline void cudaFftThrowOnError(cufftResult err)
{
    switch (err)
    {
        case CUFFT_SUCCESS:
            break;
        case CUFFT_ALLOC_FAILED:
            throw std::bad_alloc();
        default:
            throw std::runtime_error(cudaFftErrorString(err));
    }
}

inline void cudaFftLogError(cufftResult err)
{
    if (err != CUFFT_SUCCESS)
    {
        LOG(error) << "CUDA FFT error: " << cudaFftErrorString(err);
    }
}

// defines a single-precision complex-FFT
class CudaFft
{
public:
    CudaFft()
    {
        cudaFftThrowOnError( cufftCreate(&plan_) );
    }
    
    ~CudaFft()
    {
        cudaFftLogError( cufftDestroy(plan_) );
    }

    // no copying allowed
    CudaFft(const CudaFft&) = delete;
    CudaFft& operator=(const CudaFft&) = delete;
    
    // moving is allowed
    CudaFft(CudaFft&&) = default;
    CudaFft& operator=(CudaFft&&) = default;
    
    // creates a batch of 'dim2' continguous 1-D FFTs of size 'dim1'
    // returns the size of the work area
    size_t plan1d(int dim1, int dim2)
    {
        size_t workSize;
        cudaFftThrowOnError( cufftMakePlan1d( plan_, dim1, CUFFT_C2C, dim2, &workSize ) );
        return workSize;
    }

    //// Add more plans here as needed -- use cudaMakePlanXXX form

    // performs the FFT, set 'in' equal to 'out' to perform an in-place FFT
    void operator()(const cuComplex* in, cuComplex* out, bool inverseFlag=false) const
    {
        auto flag = inverseFlag ? CUFFT_INVERSE : CUFFT_FORWARD;
        cudaFftThrowOnError( cufftExecC2C( plan_, const_cast<cuComplex*>(in), out, flag ) );
    }

private:
    cufftHandle plan_;
};
