#pragma once
#include "CudaError.h"

// RAII for Cuda Events
struct CudaEvent
{
    struct NotReadyError : std::exception
    { 
        const char* what() const noexcept { return "Cuda Event Not Ready"; }
    };

    struct NotRecordedError : std::exception
    { 
        const char* what() const noexcept { return "Cuda Event Not Recorded"; }
    };

    CudaEvent(bool disableTiming=false, bool blockingSync=false)
    {
        unsigned flags = (disableTiming ? cudaEventDisableTiming : 0)
                       | (blockingSync  ? cudaEventBlockingSync  : 0);
        cudaThrowOnError(cudaEventCreate(&obj, flags));
    }

    ~CudaEvent() { cudaEventDestroy(obj); }

    cudaEvent_t get() { return obj; }

    void record() { cudaThrowOnError(cudaEventRecord(obj)); }
    void sync()   { cudaThrowOnError(cudaEventSynchronize(obj)); }

    bool done()
    {
        auto query = cudaEventQuery(obj);
        if (query == cudaErrorNotReady) return false;
        cudaThrowOnError(query);
        return true;
    }

    float elapsedTime(CudaEvent& until)
    {
        float milliseconds;
        auto err = cudaEventElapsedTime(&milliseconds, obj, until.obj);
        if (err == cudaErrorInvalidResourceHandle)
        {
            throw NotRecordedError();
        }
        if (err == cudaErrorNotReady)
        {
            throw NotReadyError();
        }
        cudaThrowOnError(err);
        return milliseconds/1000.f;
    }

private:
    cudaEvent_t obj;
};

// RAII Object for cuda timing
struct CudaTimer
{
    CudaTimer()  { reset(); }
    void reset() { start_.record(); }
    void stop()  {  stop_.record(); }
    float et()
    {
        stop_.sync();
        return start_.elapsedTime(stop_);
    }
    float toc() { stop(); return et(); }
private:
    CudaEvent start_;
    CudaEvent stop_;
};

