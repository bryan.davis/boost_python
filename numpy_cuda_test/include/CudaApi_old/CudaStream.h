#pragma once
#include "CudaError.h"

// defines an RIAA wrapper around CUDA streams
class CudaStream
{
public:
    CudaStream(bool nonBlockingWithDefaultStream=false, int priority=0)
    {
        auto flags = nonBlockingWithDefaultStream ? cudaStreamNonBlocking : cudaStreamDefault;
        cudaThrowOnError( cudaStreamCreateWithPriority( &stream_, flags, priority ));
    }

    static CudaStream defaultStream()
    {
        return CudaStream(nullptr);
    }

private:
    CudaStream(void *private_func)
    {
        stream_ = nullptr;
    }

public:
    ~CudaStream() noexcept
    {
        if (stream_)
        {
            auto status = cudaStreamDestroy(stream_);
            cudaLogError(status);
        }
    }

    void sync()
    {
        cudaThrowOnError(cudaStreamSynchronize(stream_));
    }

    bool done() const
    {
        auto query = cudaStreamQuery(stream_);
        if (query == cudaErrorNotReady) return false;
        cudaThrowOnError(query);
        return true;
    }

    cudaStream_t get()      const { return stream_; }
    operator cudaStream_t() const { return stream_; }

private:
    cudaStream_t stream_;
};

