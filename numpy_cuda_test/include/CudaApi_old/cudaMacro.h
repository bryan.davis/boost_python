//============================================================
// Proprietary and confidential
//    Copyright (C) Fortem Technologies, Inc - All Rights Reserved Worldwide
//    Unauthorized copying of this file, via any medium is strictly prohibited
//============================================================

#pragma once

#include <exceptions.h>

// Macro for checking cuda errors following a cuda launch or api call
// Based on Jeff Larkin's code
#define cudaCheckError() {                                         \
 cudaError_t e=cudaGetLastError();                                 \
 if(e!=cudaSuccess) {                                              \
   throw CudaError(__FILE__, __LINE__, cudaGetErrorString(e));     \
 }                                                                 \
}


#define CCE(X) X; \
    cudaCheckError();

