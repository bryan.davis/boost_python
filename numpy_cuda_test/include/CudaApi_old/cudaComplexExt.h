//============================================================
// Proprietary and confidential
//    Copyright (C) Fortem Technologies, Inc - All Rights Reserved Worldwide
//    Unauthorized copying of this file, via any medium is strictly prohibited
//============================================================
#pragma once

#ifdef __CUDACC__
#include <cuComplex.h>
#else
#include </usr/local/cuda/include/cuComplex.h>
#endif
#include <ostream>
#include <complex>

// Include functions for intuitive complex float operations in cuda device code
__device__ __host__ inline cuComplex operator-(const cuComplex& rhs)
{
    return make_cuComplex(-rhs.x, -rhs.y);
}

__device__ __host__ inline cuComplex& operator+=(cuComplex& lhs, const cuComplex& rhs)
{
    lhs = cuCaddf(lhs,rhs);
    return lhs;
}

__device__ __host__ inline cuComplex& operator+=(cuComplex& lhs, const float& rhs)
{
    lhs.x += rhs;
    return lhs;
}

__device__ __host__ inline cuComplex operator+(const cuComplex& lhs, const cuComplex& rhs)
{
    return cuCaddf(lhs,rhs);
}

__device__ __host__ inline cuComplex operator+(cuComplex lhs, float rhs)
{
    return lhs += rhs;
}

__device__ __host__ inline cuComplex operator+(float lhs, cuComplex rhs)
{
    return rhs += lhs;
}

__device__ __host__ inline cuComplex& operator-=(cuComplex& lhs, const cuComplex& rhs)
{
    lhs = cuCsubf(lhs,rhs);
    return lhs;
}

__device__ __host__ inline cuComplex& operator-=(cuComplex& lhs, const float& rhs)
{
    lhs.x -= rhs;
    return lhs;
}

__device__ __host__ inline cuComplex operator-(const cuComplex& lhs, const cuComplex& rhs)
{
    return cuCsubf(lhs,rhs);
}

__device__ __host__ inline cuComplex operator-(cuComplex lhs, float rhs)
{
    return lhs -= rhs;
}

__device__ __host__ inline cuComplex operator-(float lhs, cuComplex rhs)
{
    return -(rhs -= lhs);
}

__device__ __host__ inline cuComplex& operator*=(cuComplex& lhs, const cuComplex& rhs)
{
    lhs = cuCmulf(lhs,rhs);
    return lhs;
}

__device__ __host__ inline cuComplex operator*(const cuComplex& lhs, const cuComplex& rhs)
{
    return cuCmulf(lhs,rhs);
}

__device__ __host__ inline cuComplex& operator/=(cuComplex& lhs, const cuComplex& rhs)
{
    lhs = cuCdivf(lhs,rhs);
    return lhs;
}

__device__ __host__ inline cuComplex operator/(const cuComplex& lhs, const cuComplex& rhs)
{
    return cuCdivf(lhs,rhs);
}

__device__ __host__ inline cuComplex& operator*=(cuComplex& lhs, const float& rhs)
{
    lhs.x *= rhs;
    lhs.y *= rhs;
    return lhs;
}

__device__ __host__ inline cuComplex operator*(const cuComplex& lhs, const float& rhs)
{
    return make_cuComplex(lhs.x*rhs, lhs.y*rhs);
}

__device__ __host__ inline cuComplex operator*(const float& lhs, const cuComplex& rhs)
{
    return make_cuComplex(rhs.x*lhs, rhs.y*lhs);
}

__device__ __host__ inline cuComplex& operator/=(cuComplex& lhs, const float& rhs)
{
    lhs.x /= rhs;
    lhs.y /= rhs;
    return lhs;
}

__device__ __host__ inline cuComplex operator/(const cuComplex& lhs, const float& rhs)
{
    return make_cuComplex(lhs.x/rhs, lhs.y/rhs);
}

__device__ __host__ inline float magSq(const cuComplex& x)
{
    return x.x*x.x + x.y*x.y;
}

__device__ __host__ inline float mag(const cuComplex& x)
{
    return sqrt(x.x*x.x + x.y*x.y);
}

__device__ __host__ inline cuComplex conj(const cuComplex& x)
{
    return cuConjf(x);
}

__device__ __host__ inline float real(const cuComplex& x)
{
    return x.x;
}

__device__ __host__ inline float imag(const cuComplex& x)
{
    return x.y;
}

// returns c = a * conj(b)
__device__ __host__ inline cuComplex multConj(const cuComplex& lhs, const cuComplex& rhs)
{
    cuComplex prod;
    prod.x = lhs.x * rhs.x
           + lhs.y * rhs.y;
    prod.y = lhs.y * rhs.x 
           - lhs.x * rhs.y;
    return prod;
}

__host__ inline const cuComplex& toCuda(const std::complex<float>& x)
{
    return reinterpret_cast<const cuComplex&>(x);
}

__host__ inline cuComplex& toCuda(std::complex<float>& x)
{
    return reinterpret_cast<cuComplex&>(x);
}

__host__ inline cuComplex&& toCuda(std::complex<float>&& x)
{
    return reinterpret_cast<cuComplex&&>(x);
}

#ifdef __CUDACC__
__device__ __host__ inline cuComplex phasor(float angle)
{
    cuComplex phasor;
    sincosf(angle, &phasor.y, &phasor.x);
    return phasor;
}

__device__ __host__ inline cuComplex piPhasor(float piAngle)
{
    cuComplex phasor;
    sincospif(piAngle, &phasor.y, &phasor.x);
    return phasor;
}
#else
inline cuComplex phasor(float angle)
{
    return toCuda(std::polar(1.f, angle));
}

inline cuComplex piPhasor(float piAngle)
{
    return phasor(piAngle*PIf);
}
#endif

__host__ inline std::ostream& operator<<(std::ostream& out, const cuComplex& rhs)
{
    out << rhs.x << "+" << rhs.y << "j";
    return out;
}

// Include functions for intuitive complex double operations in cuda device code
__device__ __host__ inline cuDoubleComplex operator-(const cuDoubleComplex& rhs)
{
    return make_cuDoubleComplex(-rhs.x, -rhs.y);
}

__device__ __host__ inline cuDoubleComplex& operator+=(cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    lhs = cuCadd(lhs,rhs);
    return lhs;
}

__device__ __host__ inline cuDoubleComplex operator+(const cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    return cuCadd(lhs,rhs);
}

__device__ __host__ inline cuDoubleComplex& operator-=(cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    lhs = cuCsub(lhs,rhs);
    return lhs;
}

__device__ __host__ inline cuDoubleComplex operator-(const cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    return cuCsub(lhs,rhs);
}

__device__ __host__ inline cuDoubleComplex& operator*=(cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    lhs = cuCmul(lhs,rhs);
    return lhs;
}

__device__ __host__ inline cuDoubleComplex operator*(const cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    return cuCmul(lhs,rhs);
}

__device__ __host__ inline cuDoubleComplex& operator/=(cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    lhs = cuCdiv(lhs,rhs);
    return lhs;
}

__device__ __host__ inline cuDoubleComplex operator/(const cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    return cuCdiv(lhs,rhs);
}

__device__ __host__ inline cuDoubleComplex& operator*=(cuDoubleComplex& lhs, const double& rhs)
{
    lhs.x *= rhs;
    lhs.y *= rhs;
    return lhs;
}

__device__ __host__ inline cuDoubleComplex operator*(const cuDoubleComplex& lhs, const double& rhs)
{
    return make_cuDoubleComplex(lhs.x*rhs, lhs.y*rhs);
}

__device__ __host__ inline cuDoubleComplex operator*(const double& lhs, const cuDoubleComplex& rhs)
{
    return make_cuDoubleComplex(rhs.x*lhs, rhs.y*lhs);
}

__device__ __host__ inline cuDoubleComplex& operator/=(cuDoubleComplex& lhs, const double& rhs)
{
    lhs.x /= rhs;
    lhs.y /= rhs;
    return lhs;
}

__device__ __host__ inline cuDoubleComplex operator/(const cuDoubleComplex& lhs, const double& rhs)
{
    return make_cuDoubleComplex(lhs.x/rhs, lhs.y/rhs);
}

__device__ __host__ inline double magSq(const cuDoubleComplex& x)
{
    return x.x*x.x + x.y*x.y;
}

__device__ __host__ inline double mag(const cuDoubleComplex& x)
{
    return sqrt(x.x*x.x + x.y*x.y);
}

__device__ __host__ inline cuDoubleComplex conj(const cuDoubleComplex& x)
{
    return cuConj(x);
}

__device__ __host__ inline double real(const cuDoubleComplex& x)
{
    return x.x;
}

__device__ __host__ inline double imag(const cuDoubleComplex& x)
{
    return x.y;
}

// returns c = a * conj(b)
__device__ __host__ inline cuDoubleComplex multConj(const cuDoubleComplex& lhs, const cuDoubleComplex& rhs)
{
    cuDoubleComplex prod;
    prod.x = lhs.x * rhs.x
             + lhs.y * rhs.y;
    prod.y = lhs.y * rhs.x
             - lhs.x * rhs.y;
    return prod;
}

__host__ inline std::ostream& operator<<(std::ostream& out, const cuDoubleComplex& rhs)
{
    out << rhs.x << "+" << rhs.y << "j";
    return out;
}

__device__ __host__ inline cuComplex toFloat(const cuDoubleComplex& a)
{
    return make_cuComplex(a.x, a.y);
}

__device__ __host__ inline cuDoubleComplex toDouble(const cuComplex& a)
{
    return make_cuDoubleComplex(a.x, a.y);
}

__host__ inline const cuDoubleComplex& toCuda(const std::complex<double>& x)
{
    return reinterpret_cast<const cuDoubleComplex&>(x);
}

__host__ inline cuDoubleComplex& toCuda(std::complex<double>& x)
{
    return reinterpret_cast<cuDoubleComplex&>(x);
}

__host__ inline cuDoubleComplex&& toCuda(std::complex<double>&& x)
{
    return reinterpret_cast<cuDoubleComplex&&>(x);
}

