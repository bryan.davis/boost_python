#include <cuda.h>
#include "CudaApi/CudaPtr.h"

void CudaDeleter::operator()(void* p)
{
    if (p) cudaFree(p);
}
