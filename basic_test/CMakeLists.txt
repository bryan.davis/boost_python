cmake_minimum_required(VERSION 3.5.0)

project(hello_ext)

find_package(PythonLibs 3.6 REQUIRED)
find_package(Boost 1.54.0 REQUIRED)

add_library (
    hello_ext SHARED
    hello.cpp
)

target_compile_features (
    hello_ext
    PUBLIC
        cxx_std_11
)

target_compile_options (
    hello_ext
    PRIVATE
        -Wall
)

include_directories( 
    ../../../include 
    ${PYTHON_INCLUDE_DIRS} 
    ${Boost_INCLUDE_DIRS} 
)

target_link_libraries(
    hello_ext
    python3.6m
    boost_python3 
)

set_target_properties(
   hello_ext
   PROPERTIES PREFIX "")

