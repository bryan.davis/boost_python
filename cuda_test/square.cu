#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <cuda.h>

inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}
#define CCE(ans) { gpuAssert((ans), __FILE__, __LINE__); }

__global__ void cudaSquare(float* in, float* out, int size)
{
    int const k = threadIdx.x + blockDim.x*blockIdx.x;
    if (k < size)
        out[k] = in[k]*in[k];
}

float square(float x)
{
    float *in, *out;
    CCE( cudaMalloc((void**)&in,  sizeof(float)) );
    CCE( cudaMalloc((void**)&out, sizeof(float)) );
    CCE( cudaMemcpy(in,&x,sizeof(float),cudaMemcpyHostToDevice) );
    cudaSquare<<<1,1>>>(in,out,1);
    float y;
    CCE( cudaMemcpy(&y,out,sizeof(float),cudaMemcpyDeviceToHost) );
    return y;
}

BOOST_PYTHON_MODULE(square_ext)
{
    using namespace boost::python;
    def("square", square);
}

